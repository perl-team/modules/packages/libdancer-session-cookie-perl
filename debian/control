Source: libdancer-session-cookie-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Alexandre Mestiashvili <mestia@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libcrypt-cbc-perl <!nocheck>,
                     libcrypt-rijndael-perl <!nocheck>,
                     libdancer-perl <!nocheck>,
                     libhttp-cookies-perl <!nocheck>,
                     libhttp-message-perl <!nocheck>,
                     libperlx-maybe-perl <!nocheck>,
                     libplack-perl <!nocheck>,
                     libsession-storage-secure-perl (>= 1.000) <!nocheck>,
                     libstring-crc32-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-nowarnings-perl <!nocheck>,
                     libtest-requires-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libtime-duration-parse-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdancer-session-cookie-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdancer-session-cookie-perl.git
Homepage: https://metacpan.org/release/Dancer-Session-Cookie
Rules-Requires-Root: no

Package: libdancer-session-cookie-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcrypt-cbc-perl,
         libcrypt-rijndael-perl,
         libdancer-perl,
         libperlx-maybe-perl,
         libsession-storage-secure-perl (>= 1.000),
         libstring-crc32-perl,
         libtime-duration-parse-perl
Description: encrypted cookie-based session backend for Dancer
 Dancer::Session::Cookie implements a session engine for sessions stored
 entirely in cookies. Usually only session id is stored in cookies and the
 session data itself is saved in some external storage, e.g. database. This
 module allows one to avoid using external storage at all.
 .
 Since server cannot trust any data returned by client in cookies, this module
 uses cryptography to ensure integrity and also secrecy. The data your
 application stores in sessions is completely protected from both tampering
 and analysis on the client-side.
